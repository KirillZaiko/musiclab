import _ from 'lodash'

const rStep = (200 - 50) / 8
const gStep = (200 - 150) / 8
const bStep = (200 - 50) / 8

export const greenPalette = _.range(7).map(
  (index) => `rgb(${50 + index * rStep}, ${150 + index * gStep}, ${50 + index * bStep})`
)

export const green = greenPalette[0]
export const red = '#ff0000'
export const gray = '#bfbfbf'

export const acidicGreen = '#4dbb82'
export const acidicTonic = '#36e232'
export const acidicSubdominant = '#69b4ee'
export const acidicDominant = '#bf83f6'
export const acidicPalette = [
  acidicTonic,
  acidicGreen,
  acidicGreen,
  acidicSubdominant,
  acidicDominant,
  acidicGreen,
  acidicGreen,
]

export const rainbowPalette = [
  '#ff0000',
  '#ffa500',
  '#ffff00',
  '#28ce28',
  '#7b7bff',
  '#bf67fd',
  '#f47ed7',
]

export const rainbow12Palette = [
  '#ff0000',
  '#ff8000',
  '#ffff00',
  '#80ff00',
  '#00ff00',
  '#00ff80',
  '#00ffff',
  '#0080ff',
  '#0000ff',
  '#8000ff',
  '#ff00ff',
  '#ff0080',
]

export function hexToRgb(hex) {
  // Convert the hex string to a base-16 integer
  if (hex.startsWith('#')) {
    hex = hex.slice(1)
  }
  const intValue = parseInt(hex, 16)

  const red = (intValue >> 16) & 0xff
  const green = (intValue >> 8) & 0xff
  const blue = intValue & 0xff

  return [red, green, blue]
}

export const allPalettes = {
  rainbow: rainbowPalette,
  green: greenPalette,
  acidic: acidicPalette,
}
