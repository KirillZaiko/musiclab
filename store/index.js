import * as colorsModule from '@/utils/colors'

export const state = {
  instrument: 'piano',
  playOnPress: false,
  midi: {
    pressedNotes: [],
  },
  paletteName: 'rainbow',
}

export const mutations = {
  SET_INSTRUMENT(state, instrument) {
    state.instrument = instrument
  },
  SET_PLAY_ON_PRESS(state, playOnPress) {
    state.playOnPress = playOnPress
  },
  MIDI_PRESSED_NOTE_ADD(state, note) {
    state.midi.pressedNotes = [...state.midi.pressedNotes, note]
  },
  MIDI_PRESSED_NOTE_REMOVE(state, note) {
    state.midi.pressedNotes = state.midi.pressedNotes.filter((x) => x !== note)
  },
  MIDI_PRESSED_NOTE_CLEAR(state) {
    state.midi.pressedNotes = []
  },
  SET_PALETTE_NAME(state, paletteName) {
    state.paletteName = paletteName
  },
}

export const actions = {
  setInstrument(context, instrument) {
    context.commit('SET_INSTRUMENT', instrument)
  },
}

export const getters = {
  allInstruments() {
    return ['piano', 'guitar', 'ukulele']
  },
  palette(state) {
    return colorsModule.allPalettes[state.paletteName]
  },
}
