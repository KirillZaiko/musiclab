import _ from 'lodash'

export const allNotes = Object.freeze([
  'C',
  'C#',
  'D',
  'D#',
  'E',
  'F',
  'F#',
  'G',
  'G#',
  'A',
  'A#',
  'B',
])
export const octaves = Object.freeze(_.range(0, 8))
export const allFullNotes = Object.freeze(
  _.flatMap(octaves, (octave) => {
    return allNotes.map((note) => `${note}${octave}`)
  })
)
export const patterns = Object.freeze({
  // todo deepFreeze
  '': [0, 4, 7],
  m: [0, 3, 7],
  7: [0, 4, 7, 10],
  maj7: [0, 4, 7, 11],
  m7: [0, 3, 7, 10],
  dim: [0, 3, 6],
  dim7: [0, 3, 6, 9],
  sus4: [0, 5, 7],
  sus2: [0, 2, 7],
  '7sus4': [0, 5, 7, 10],
  '7sus2': [0, 2, 7, 10],
})
export const scalePatterns = Object.freeze({
  // todo deepFreeze
  minor: [0, 2, 3, 5, 7, 8, 10],
  major: [0, 2, 4, 5, 7, 9, 11],
})
export const getLetter = (pitchN) => {
  return allNotes[pitchN % allNotes.length]
}
export const fromLetter = (letter) => {
  return allNotes.indexOf(letter)
}
export const midiPitchToNote = (midiPitch) => {
  // 24 == C0
  // 107 == B6
  if (midiPitch >= 24 <= 108) {
    return noteIncr('C0', midiPitch - 24)
  } else {
    return null
  }
}
export const getAllFullNotesRange = (startNote, endNote) => {
  const startNoteI = allFullNotes.indexOf(startNote)
  const endNoteI = allFullNotes.indexOf(endNote)
  return allFullNotes.slice(startNoteI, endNoteI + 1)
}
export const noteIncr = (note, increment) => {
  const newIndex = allFullNotes.indexOf(note) + increment
  return allFullNotes[newIndex]
}
export const flatToSharp = (note) => {
  if (note[1] === 'b') {
    const index = allNotes.indexOf(note[0])
    return allNotes[index - 1]
  } else {
    return note
  }
}
export const inverseAccidental = (note) => {
  if (note[1] === 'b') {
    const index = allNotes.indexOf(note[0])
    return allNotes[index - 1] + note.slice(2)
  } else if (note[1] === '#') {
    const index = allNotes.indexOf(note[0] + '#')
    return allNotes[index + 1] + 'b' + note.slice(2)
  } else {
    return note
  }
}
export const parseChord = (chord) => {
  let root = null
  let quality = null
  if (!chord) {
    return null
  } else if (chord[1] === '#') {
    root = chord.slice(0, 2).toUpperCase()
    quality = chord.slice(2)
  } else if (chord[1] === 'b') {
    root = chord.slice(0, 2)
    root = flatToSharp(root)
    quality = chord.slice(2)
  } else {
    root = chord[0].toUpperCase()
    quality = chord.slice(1)
  }
  if (!allNotes.includes(root) || !(quality in patterns)) {
    return null
  }
  return { root, quality }
}
export const getChordNotes = (chordRoot, chordQuality) => {
  return getPitchList(chordRoot, chordQuality).map((x) => getLetter(x))
}
export const getChordFullNotes = (fullRoot, chordQuality) => {
  if (patterns[chordQuality] && allFullNotes.includes(fullRoot)) {
    return patterns[chordQuality].map((x) => noteIncr(fullRoot, x))
  } else {
    return []
  }
}
export const getPitchList = (chordRoot, chordQuality) => {
  if (patterns[chordQuality] && fromLetter(chordRoot) > -1) {
    const rootN = fromLetter(chordRoot)
    return patterns[chordQuality].map((x) => rootN + x)
  } else {
    return []
  }
}
export const getScale = (root, scaleName) => {
  if (!scalePatterns[scaleName]) {
    return null
  }
  const rootN = fromLetter(root)
  return scalePatterns[scaleName].map((offset) => getLetter(rootN + offset))
}
export const getScaleByChord = (chord) => {
  const { root, quality } = parseChord(chord)
  if (!root) {
    return null
  }
  const minor = quality.startsWith('m') && !quality.startsWith('maj')
  const scaleName = minor ? 'minor' : 'major'
  return getScale(root, scaleName)
}
export const getScaleChords = (scale) => {
  const scaleNotes = getScale(scale.root, scale.mode)
  let scaleChords

  if (scale.mode === 'minor') {
    const minorChordQuality = ['m', 'dim', '', 'm', 'm', '', '']
    scaleChords = scaleNotes.map((x, index) => x + minorChordQuality[index])
  } else if (scale.mode === 'major') {
    const majorChordQuality = ['', 'm', 'm', '', '', 'm', 'dim']
    scaleChords = scaleNotes.map((x, index) => x + majorChordQuality[index])
  } else {
    throw new Error(`Unknown scale mode '${scale.mode}'`)
  }

  return scaleChords
}
export const parseScale = (scale) => {
  const { root, quality } = parseChord(scale)
  if (!root) {
    return null
  }
  const minor = quality.startsWith('m') && !quality.startsWith('maj')
  const mode = minor ? 'minor' : 'major'
  return { root, mode }
}

export default {
  allNotes,
  octaves,
  allFullNotes,
  patterns,
  scalePatterns,
  getLetter,
  fromLetter,
  midiPitchToNote,
  getAllFullNotesRange,
  noteIncr,
  flatToSharp,
  inverseAccidental,
  parseChord,
  getChordNotes,
  getChordFullNotes,
  getPitchList,
  getScale,
  getScaleByChord,
  getScaleChords,
  parseScale,
}
